package com.ngmhjp.shiplog;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;


import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.gson.Gson;
import com.ngmhjp.shiplog.adapters.RecyclerAdapter;
import com.ngmhjp.shiplog.adapters.RecyclerItemClickListener;
import com.ngmhjp.shiplog.authentification.SignInActivity;
import com.ngmhjp.shiplog.model.Friend;
import com.ngmhjp.shiplog.model.User;
import com.ngmhjp.shiplog.network.AsyncResponse;
import com.ngmhjp.shiplog.network.FirebaseAsyncTask;

import java.util.ArrayList;
import java.util.List;


public class FriendsListActivity extends AppCompatActivity implements View.OnClickListener, RecyclerItemClickListener.OnItemClickListener, AsyncResponse {

    private FirebaseAuth firebaseAuth;
    private RecyclerView mRecyclerView;
    private RecyclerAdapter mAdapter;
    private LinearLayoutManager mLayoutManager;
    public static User LOGGEDINUSER;
    SwipeRefreshLayout mSwipeRefreshLayout;
    private static final int MY_PERMISSIONS_REQUEST_STORAGE = 100;
    private static final int ADD_NEW_FRIEND_REQUESTCODE = 200;
    private static final int VIEW_FRIEND_REQUESTCODE = 300;
    FirebaseAsyncTask mAsyncTask =new FirebaseAsyncTask();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i("FriendsListActivity", "onCreate() method called");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friends_list);

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (ContextCompat.checkSelfPermission(getApplicationContext(),
                    Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    Toast.makeText(getApplicationContext(), R.string.permissionString, Toast.LENGTH_LONG).show();
                } else {
                    // No explanation needed, we can request the permission.
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            MY_PERMISSIONS_REQUEST_STORAGE);
                }
            }
        }

        // ------- Initialize Firebase Database and User

        firebaseAuth = firebaseAuth.getInstance();
        FirebaseUser user = firebaseAuth.getCurrentUser();

        if (user == null) {
            finish();
            startActivity(new Intent(getApplicationContext(), SignInActivity.class));
        }

        SharedPreferences mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        Gson gson = new Gson();
        String json;
        SharedPreferences.Editor editor = mSharedPreferences.edit();

        boolean justRegistered = mSharedPreferences.getBoolean("JustRegistered", false);
        boolean wasOffline = mSharedPreferences.getBoolean("WasOffline", false);
        boolean loadRecyclerView = true;

        // -- User just registered. Create new LOGGEDINUSER instance
        if (justRegistered) {
            editor.putBoolean("JustRegistered", false);
            editor.commit();

            json = mSharedPreferences.getString("NewUser", "");
            Friend UserFriend = gson.fromJson(json, Friend.class);
            List<Friend> list = new ArrayList<Friend>();
            list.add(UserFriend);
            LOGGEDINUSER = new User(this, user.getEmail(), user.getUid());
            LOGGEDINUSER.setmFriendslist(list);

        // -- User just signed in with an existing account. Download the user from Firebase
        } else if (wasOffline) {
            editor.putBoolean("WasOffline", false);
            editor.commit();

            mAsyncTask.delegate = this;
            mAsyncTask.execute(user.getUid(), user.getEmail());
            loadRecyclerView = false;

        // -- User just reopened the app. Retrieve data from SharedPrefs
        } else {
            gson = new Gson();
            json = mSharedPreferences.getString("LoggedInUser", "");
            LOGGEDINUSER = gson.fromJson(json, User.class);
        }

        // ------- Load Views and make Activity look pretty
        ImageView mBackgroundPhoto = (ImageView) findViewById(R.id.friendslist_background_image);
        Glide.with(this).load(R.drawable.login_photo2).into(mBackgroundPhoto);
        ActionBar ab = getSupportActionBar();
        ab.setTitle("Your Friends");


        // -- FAB
        FloatingActionButton fabAddFriend = (FloatingActionButton) findViewById(R.id.fab_add);
        fabAddFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO Doesn't onPause() take care of saving?
                saveUserToSharedPrefs();
                startActivityForResult(new Intent(getApplicationContext(), ProfileCreationActivity.class), ADD_NEW_FRIEND_REQUESTCODE);
            }
        });

        // -- Recycler View
        if (loadRecyclerView) {
            mAdapter = new RecyclerAdapter(LOGGEDINUSER.getmFriendslist(), this);
            mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view_friends);
            mLayoutManager = new LinearLayoutManager(this);

            mRecyclerView.setLayoutManager(mLayoutManager);
            mRecyclerView.setAdapter(mAdapter);

            mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, this));
        }

        //=====================swipe to refresh======================
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshItems(LOGGEDINUSER.mFriendslist);

            }
        });

    }

    /**
     * This method saves the current LOGGEDINUSER to SharedPreferences in JSON.
     * Get object with key "LoggedInUser"
     */
    private void saveUserToSharedPrefs() {
        SharedPreferences mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(LOGGEDINUSER);
        editor.putString("LoggedInUser", json);
        editor.commit();
    }

    /**
     * Refreshes Friendslist View
     * @param list old list to compare current Listview with
     */
    private void refreshItems(List list) {
        List listNew = LOGGEDINUSER.getmFriendslist();

        if (listNew.equals(list)) {
            mSwipeRefreshLayout.setRefreshing(false);
            Toast.makeText(this, R.string.nothing_to_change, Toast.LENGTH_SHORT).show();
            return;
        } else {
            mAdapter = new RecyclerAdapter(LOGGEDINUSER.getmFriendslist(), this);
            onItemsLoadComplete();
        }
    }

    /**
     * This method resets all RecyclerView components to update the list
     */
    private void onItemsLoadComplete() {
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view_friends);
        mLayoutManager = new LinearLayoutManager(this);

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);

        mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, this));
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onItemClick(View childView, int position) {
        //Toast.makeText(FriendsListActivity.this, position + " clicked", Toast.LENGTH_SHORT).show();

        //Log Name of clicked Friend for Debug Purposes
        Friend item = LOGGEDINUSER.getmFriendslist().get(position);
        Log.i("CLICKED: ", item.writeFullName());

        //Send User & position to ProfileActivity
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(LOGGEDINUSER);
        editor.putString("LoggedInUser", json);
        editor.putInt("Index", position);
        editor.commit();

        //Start ProfileActivity
        startActivityForResult(new Intent(getApplicationContext(), ProfileActivity.class), VIEW_FRIEND_REQUESTCODE);
    }

    @Override
    public void onItemLongPress(View childView, int position) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_about:
                boolean wrapInScrollView = true;
                new MaterialDialog.Builder(this)
                        .title(R.string.about)
                        .customView(R.layout.about_section, wrapInScrollView)
                        .neutralText(R.string.website)
                        .onNeutral(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                View v = dialog.getView();
                                Uri uri = Uri.parse("http://codepod.eu/shiplog");
                                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                startActivity(intent);
                            }
                        })
                        .positiveText(R.string.okay)
                        .show();
                return true;
            case R.id.action_settings:
                startActivity(new Intent(getApplicationContext(), SettingsActivity.class));
                return true;
            case R.id.action_sign_out:
                firebaseAuth.signOut();
                SharedPreferences mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
                SharedPreferences.Editor editor = mSharedPreferences.edit();
                editor.putBoolean("WasOffline", true);
                editor.commit();

                finish();
                startActivity(new Intent(getApplicationContext(), SignInActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_STORAGE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //permission granted
                } else {
                    //permission denied. disable function
                }
                return;

            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        Gson gson = new Gson();

        if (requestCode == ADD_NEW_FRIEND_REQUESTCODE) {
            if (resultCode == Activity.RESULT_OK) {
                // -- Get newly created Friend from previous Activity via JSON +
                String json = sharedPreferences.getString("NewFriend", "");
                Friend newFriend = gson.fromJson(json, Friend.class);
                LOGGEDINUSER.mFriendslist.add(newFriend);

                mAdapter = new RecyclerAdapter(LOGGEDINUSER.getmFriendslist(), this);
                onItemsLoadComplete();
            }

        } else if (requestCode == VIEW_FRIEND_REQUESTCODE) {
            Log.i("FriendsListActivity", "updating LOGGEDINUSER for changes...");
            // -- Get newly created LoggedInUser from ProfileActivity and update List
            String json = sharedPreferences.getString("LoggedInUser", "");
            User updatedUser = gson.fromJson(json, User.class);
            LOGGEDINUSER = updatedUser;

            mAdapter = new RecyclerAdapter(LOGGEDINUSER.getmFriendslist(), this);
            onItemsLoadComplete();
        }

    }

    @Override
    protected void onPause() {
        super.onStop();
        // --- BackUp the User before the Activity get's closed/destroyed
        Log.i("FriendsListActivity", "onPause() method called");
        saveUserToSharedPrefs();
    }

    /**
     * Method get's called as soon as Firebase Download is complete.
     * Set's RecyclerViews and updates the Friendslist-View upon completion.
     * @param output Downloaded LOGGEDINUSER Object
     */
    @Override
    public void processFinish(User output) {
        Log.i("FriendsListActivity", "processFinish() method entered");
        LOGGEDINUSER = output;
        LOGGEDINUSER.setmUID(firebaseAuth.getCurrentUser().getUid());
        mAdapter = new RecyclerAdapter(LOGGEDINUSER.getmFriendslist(), this);
        onItemsLoadComplete();
    }
}
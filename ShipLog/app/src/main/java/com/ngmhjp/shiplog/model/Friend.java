package com.ngmhjp.shiplog.model;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import java.io.ByteArrayOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Friend {
    private String mBirthdayStr;

    private String mFirstName;
    private String mLastName;

    private String mStarsign;

    private String mEduInstitution;
    private String mEduGraduationStr;
    private String mEduMajor;

    private String mJobEmployer;
    private String mJobPosition;

    private String mPlaceOfRes= "";

    private String mPhonenumber= "";
    private String mHeight= "";
    private String mEye= "";
    private String mHair= "";
    private String mFilm= "";
    private String mBook= "";
    private String mComic= "";
    private String mGame= "";
    private String mPet= "";
    private String mSupporter= "";

    private int mPhotoResID = 0;
    private String mProfilePicture = "";

    public Friend(String _firstName, String _lastName, Date _birthday, String _eduInstitution, Date _eduGraduation, String _eduMajor, String _jobEmployer, String _jobPosition, String _placeOfRes, String _phone, String _height, String _eye, String _hair, String _film, String _book, String _comic, String _game, String _pet, String _supporter, Bitmap _profilePicture) {
        mFirstName = _firstName;
        mLastName = _lastName;
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd. MMMM yyyy", Locale.US);
        this.mBirthdayStr = dateFormatter.format(_birthday);
        mEduInstitution = _eduInstitution;
        this.mEduGraduationStr = dateFormatter.format(_eduGraduation);
        mEduMajor = _eduMajor;
        mJobEmployer = _jobEmployer;
        mJobPosition = _jobPosition;
        mPlaceOfRes = _placeOfRes;
        mPhonenumber = _phone;
        mHeight = _height;
        mEye = _eye;
        mHair = _hair;
        mFilm = _film;
        mBook = _book;
        mComic = _comic;
        mGame = _game;
        mPet = _pet;
        mSupporter = _supporter;
        mProfilePicture = encodeToBase64(_profilePicture, Bitmap.CompressFormat.JPEG, 100);
        mStarsign = getZodiacSign();
    }

    public Friend(String _firstName, String _lastName, Date _birthday, String _eduInstitution, Date _eduGraduation, String _eduMajor, String _jobEmployer, String _jobPosition, Bitmap _profilePicture) {
        mFirstName = _firstName;
        mLastName = _lastName;
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd. MMMM yyyy", Locale.US);
        this.mBirthdayStr = dateFormatter.format(_birthday);
        mEduInstitution = _eduInstitution;
        this.mEduGraduationStr = dateFormatter.format(_eduGraduation);
        mEduMajor = _eduMajor;
        mJobEmployer = _jobEmployer;
        mJobPosition = _jobPosition;
        mProfilePicture = encodeToBase64(_profilePicture, Bitmap.CompressFormat.JPEG, 100);
        mStarsign = getZodiacSign();
    }

    public Friend() {
    }

    public String writeFullName() {
        String combinedName = mFirstName;
        combinedName += " ";
        combinedName += mLastName;
        return combinedName;
    }

    public String getmEduGraduationStr() {
        return mEduGraduationStr;
    }

    public void setmEduGraduationStr(String mEduGraduationStr) {
        this.mEduGraduationStr = mEduGraduationStr;
    }

    public Date dateEduGraduationGetter() {
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd. MMMM yyyy", Locale.US);

        try {
            return dateFormatter.parse(mEduGraduationStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getmBirthdayStr() {
        return mBirthdayStr;
    }

    public void setmBirthdayStr(String mBirthdayStr) {
        this.mBirthdayStr = mBirthdayStr;
    }

    public void dateEduGraduationSetter(Date mEduGraduation) {
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd. MMMM yyyy", Locale.US);
        this.mEduGraduationStr = dateFormatter.format(mEduGraduation);
    }

    public String getmEduMajor() {
        return mEduMajor;
    }

    public void setmEduMajor(String mEduMajor) {
        this.mEduMajor = mEduMajor;
    }

    public String getmPlaceOfRes() {
        return mPlaceOfRes;
    }

    public void setmProfilePicture(String bitmap) {
        this.mProfilePicture = bitmap;
    }

    public String getmProfilePicture() {
        return this.mProfilePicture;
    }

    public String getmFirstName() {
        return mFirstName;
    }

    public void setmFirstName(String mFirstName) {
        this.mFirstName = mFirstName;
    }

    public String getmLastName() {
        return mLastName;
    }

    public void setmLastName(String mLastName) {
        this.mLastName = mLastName;
    }

    public Date dateBirthdayGetter() {
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd. MMMM yyyy", Locale.US);

        try {
            return dateFormatter.parse(mBirthdayStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void dateBirthdaySetter(Date mBirthday) {
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd. MMMM yyyy", Locale.US);
        this.mBirthdayStr = dateFormatter.format(mBirthday);
    }

    public String getmEduInstitution() {
        return mEduInstitution;
    }

    public void setmEduInstitution(String mEduInstitution) {
        this.mEduInstitution = mEduInstitution;
    }

    public String getmJobEmployer() {
        return mJobEmployer;
    }

    public void setmJobEmployer(String mJobEmployer) {
        this.mJobEmployer = mJobEmployer;
    }

    public String getmJobPosition() {
        return mJobPosition;
    }

    public void setmJobPosition(String mJobPosition) {
        this.mJobPosition = mJobPosition;
    }

    public String getmPlaceOfResidence() {
        return mPlaceOfRes;
    }

    public void setmPlaceOfRes(String mPlaceOfRes) {
        this.mPlaceOfRes = mPlaceOfRes;
    }

    public String getmPhonenumber() {
        return mPhonenumber;
    }

    public void setmPhonenumber(String mPhonenumber) {
        this.mPhonenumber = mPhonenumber;
    }

    public String getmHeight() {
        return mHeight;
    }

    public void setmHeight(String mHeight) {
        this.mHeight = mHeight;
    }


    public String getmEye() {
        return mEye;
    }

    public void setmEye(String mEye) {
        this.mEye = mEye;
    }


    public String getmHair() {
        return mHair;
    }

    public void setmHair(String mHair) {
        this.mHair = mHair;
    }


    public String getmFilm() {
        return mFilm;
    }


    public void setmFilm(String mFilm) {
        this.mFilm = mFilm;
    }


    public String getmBook() {
        return mBook;
    }


    public void setmBook(String mBook) {
        this.mBook = mBook;
    }


    public String getmComic() {
        return mComic;
    }


    public void setmComic(String mComic) {
        this.mComic = mComic;
    }


    public String getmGame() {
        return mGame;
    }


    public void setmGame(String mGame) {
        this.mGame = mGame;
    }


    public String getmPet() {
        return mPet;
    }


    public void setmPet(String mPet) {
        this.mPet = mPet;
    }


    public String getmSupporter() {
        return mSupporter;
    }


    public void setmSupporter(String mSupporter) {
        this.mSupporter = mSupporter;
    }


    public static String encodeToBase64(Bitmap image, Bitmap.CompressFormat compressFormat, int quality) {
        ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        image.compress(compressFormat, quality, byteArrayOS);
        return Base64.encodeToString(byteArrayOS.toByteArray(), Base64.DEFAULT);
    }

    public static Bitmap decodeBase64(String input) {
        byte[] decodedBytes = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
    }

    private String getZodiacSign() {
        int month = this.dateBirthdayGetter().getMonth();
        int day = this.dateBirthdayGetter().getDay();
        String returnValue = "";

        switch (month) {
            case 1:
                if (day <= 20) {
                    returnValue = "Capricorn";
                } else {
                    returnValue = "Aquarius";
                }
                break;
            case 2:
                if (day <= 19) {
                    returnValue = "Aquarius";
                } else {
                    returnValue = "Pisces";
                }
                break;
            case 3:
                if (day <= 20) {
                    returnValue = "Pisces";
                } else {
                    returnValue = "Aries";
                }
                break;
            case 4:
                if (day <= 20) {
                    returnValue = "Aries";
                } else {
                    returnValue = "Taurus";
                }
                break;
            case 5:
                if (day <= 20) {
                    returnValue = "Taurus";
                } else {
                    returnValue = "Gemini";
                }
                break;
            case 6:
                if (day <= 21) {
                    returnValue = "Gemini";
                } else {
                    returnValue = "Cancer";
                }
                break;
            case 7:
                if (day <= 22) {
                    returnValue = "Cancer";
                } else {
                    returnValue = "Leo";
                }
                break;
            case 8:
                if (day <= 23) {
                    returnValue = "Leo";
                } else {
                    returnValue = "Virgo";
                }
                break;
            case 9:
                if (day <= 23) {
                    returnValue = "Virgo";
                } else {
                    returnValue = "Libra";
                }
                break;
            case 10:
                if (day <= 23) {
                    returnValue = "Libra";
                } else {
                    returnValue = "Scorpio";
                }
                break;
            case 11:
                if (day <= 22) {
                    returnValue = "Scorpio";
                } else {
                    returnValue = "Sagittarius";
                }
                break;
            case 12:
                if (day <= 21) {
                    returnValue = "Sagittarius";
                } else {
                    returnValue = "Capricorn";
                }
                break;
            default:
                break;
        }
        return returnValue;
    }

    public int getmPhotoResID() {
        return mPhotoResID;
    }

    public void setmPhotoResID(int mPhotoResID) {
        this.mPhotoResID = mPhotoResID;
    }

    public String getmStarsign() {
        return mStarsign;
    }

    public void setmStarsign(String mStarsign) {
        this.mStarsign = mStarsign;
    }
}

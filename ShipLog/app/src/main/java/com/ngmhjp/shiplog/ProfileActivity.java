package com.ngmhjp.shiplog;


import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.ngmhjp.shiplog.model.Friend;
import com.ngmhjp.shiplog.model.User;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class ProfileActivity extends AppCompatActivity implements View.OnClickListener {

    //===================== MEMBER =======================
    User LOGGEDINUSER;
    FloatingActionButton fab;
    Button edit_about_me;
    Button edit_questions;

    //==================== FRIEND DATA MEMBER ========================
    String firstname;
    String lastname;
    String name;
    Date birthday;
    String education;
    String major;
    String workplace;
    String job;
    String age;
    String stringDate;
    String residence;
    String phonenr;
    String height;
    String eye;
    String hair;
    String film;
    String book;
    String comic;
    String game;
    String pet;
    String support;
    Date graduation;
    String stringGraduation;
    CollapsingToolbarLayout collapsingToolbar;
    TextView tv_paragraph;
    String pic;


    //==================== ABOUT ME MEMBER ========================
    SimpleDateFormat dateFormatter = new SimpleDateFormat("dd. MMMM yyyy", Locale.US);
    DatePickerDialog setBirthdayPicker;
    DatePickerDialog setGraduationPicker;
    TextInputLayout til_firstname;
    TextInputEditText tiel_firstname;

    TextInputLayout til_lastname;
    TextInputEditText tiel_lastname;

    TextInputLayout til_birthday;
    TextInputEditText tiel_birthday;

    TextInputLayout til_workplace;
    TextInputEditText tiel_workplace;

    TextInputLayout til_jobtitle;
    TextInputEditText tiel_jobtitle;

    TextInputLayout til_education;
    TextInputEditText tiel_education;
    Calendar changeDate;
    Calendar changeGradDate;

    TextInputLayout til_major;
    TextInputEditText tiel_major;

    TextInputLayout til_graduation;
    TextInputEditText tiel_graduation;

    TextView textView_residence;
    TextView textView_phonenr;
    TextView textView_height;
    TextView textView_eye;
    TextView textView_hair;
    TextView textView_film;
    TextView textView_book;
    TextView textView_comic;
    TextView textView_game;
    TextView textView_pet;
    TextView textView_support;

    //============================= CAMERA USAGE MEMBER ===================
    private final int REQUEST_IMAGE_CAPTURE = 1;
    private final int PICK_IMAGE = 2;


    //===========================ADDITIONAL INFO MEMBER==============================
    EditText et_residence;
    EditText et_phone;
    EditText et_height;
    EditText et_eye;
    EditText et_hair;
    EditText et_film;
    EditText et_book;
    EditText et_comic;
    EditText et_game;
    EditText et_pet;
    EditText et_support;


    /**
     * The onCreate gets the Logged in User from sharedPreferences, gets the user's friendlist and sets up the view.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        //fab add picture
        fab = (FloatingActionButton) findViewById(R.id.fab_camera);
        fab.setOnClickListener(this);

        //get User & index from Shared Preferences
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        Gson gson = new Gson();
        String json = sharedPreferences.getString("LoggedInUser", "");
        LOGGEDINUSER = gson.fromJson(json, User.class);
        int position = sharedPreferences.getInt("Index", -1);

        //get friend that you clicked on in FriendListActivity
        List list = LOGGEDINUSER.getmFriendslist();
        Friend friend = (Friend) list.get(position);

        //get the paragraph from the information contained within the Friend object
        String paragraph = getParagraph(friend);

        //gets the image stored within the Friend object
        pic = friend.getmProfilePicture();

        //loads the picture into the imageview
        reloadImage(pic);

        //gets the additional info from the friend object and loads it into the corresponding textViews
        reloadQuestions(friend);

        //gets the toolbar and sets the back button
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //gets the collapsing toolbar and puts the friend's name into the title
        collapsingToolbar =
                (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle(friend.writeFullName());

        //sets the paragraph into the corresponding textview
        tv_paragraph = (TextView) findViewById(R.id.textview_paragraph);
        tv_paragraph.setText(paragraph);

        //set up view
        edit_about_me = (Button) findViewById(R.id.edit_about_me);
        edit_about_me.setOnClickListener(this);

        edit_questions = (Button) findViewById(R.id.edit_questions);
        edit_questions.setOnClickListener(this);
    }

    /**
     * This method takes the additional Information from the Friend object and sets the corresponding views with the new information.
     *
     * @param friend the Friend Object that the new information is gotten from.
     */
    public void reloadQuestions(Friend friend) {

        //get the information from the new friend object and check whether it's empty. If it is empty, it is set to "classified".
        residence = friend.getmPlaceOfResidence();
        residence = check(residence);
        phonenr = friend.getmPhonenumber();
        phonenr = check(phonenr);
        height = friend.getmHeight();
        height = check(height);
        eye = friend.getmEye();
        eye = check(eye);
        hair = friend.getmHair();
        hair = check(hair);
        film = friend.getmFilm();
        film = check(film);
        book = friend.getmBook();
        book = check(book);
        comic = friend.getmComic();
        comic = check(comic);
        game = friend.getmGame();
        game = check(game);
        pet = friend.getmPet();
        pet = check(pet);
        support = friend.getmSupporter();
        support = check(support);

        //set the new info into the corresponding views
        textView_residence = (TextView) findViewById(R.id.text_view_residence);
        textView_residence.setText(residence);
        textView_phonenr = (TextView) findViewById(R.id.text_view_phone_nr);
        textView_phonenr.setText(phonenr);
        textView_height = (TextView) findViewById(R.id.text_view_height);
        textView_height.setText(height);
        textView_eye = (TextView) findViewById(R.id.text_view_eye_color);
        textView_eye.setText(eye);
        textView_hair = (TextView) findViewById(R.id.text_view_hair_color);
        textView_hair.setText(hair);
        textView_film = (TextView) findViewById(R.id.text_view_film);
        textView_film.setText(film);
        textView_book = (TextView) findViewById(R.id.text_view_book);
        textView_book.setText(book);
        textView_comic = (TextView) findViewById(R.id.text_view_comic);
        textView_comic.setText(comic);
        textView_game = (TextView) findViewById(R.id.text_view_game);
        textView_game.setText(game);
        textView_pet = (TextView) findViewById(R.id.text_view_pet);
        textView_pet.setText(pet);
        textView_support = (TextView) findViewById(R.id.text_view_support);
        textView_support.setText(support);
    }

    /**
     * This method checks strings if they're empty. If they are they are set to "unknown". If they are filled, they are returned as they came in.
     *
     * @param string the string to be checked
     * @return if it is empty, "Unknown", if it is filled, the string parameter
     */
    private String check(String string) {
        if (string == null || string == " " || string == "") {
            return string = getResources().getString(R.string.classified);
        }
        return string;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            //back button hit
            case android.R.id.home:
                Intent returnIntent = new Intent();
                Gson gson = new Gson();
                String json = gson.toJson(LOGGEDINUSER);
                returnIntent.putExtra("LoggedInUserJSON", json);
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);


    }


    /**
     * Gets all data from SharedPreferences and puts them into a nice little paragraph.
     *
     * @param friend The Person for whom we want to make the little paragraph.
     * @return String containing the paragraph
     */
    public String getParagraph(Friend friend) {
        //get values from friend object
        firstname = friend.getmFirstName();
        name = friend.getmFirstName() + " " + friend.getmLastName();
        birthday = friend.dateBirthdayGetter();
        graduation = friend.dateEduGraduationGetter();
        major = friend.getmEduMajor();
        education = friend.getmEduInstitution();
        workplace = friend.getmJobEmployer();
        job = friend.getmJobPosition();
        age = getAge(birthday);
        stringDate = DateFormat.getDateInstance().format(birthday);
        stringGraduation = DateFormat.getDateInstance().format(graduation);
        Boolean graduated = getGraduation(graduation);

        StringBuilder builder = new StringBuilder();
        //name
        builder.append("Hello, my name is " + friend.getmFirstName());

        //if the workplace is empty, the last sentence is stopped, otherwise the job & jobtitle is printed
        if (workplace == null) {
            builder.append(". ");
        } else {
            builder.append(" and I work at " + workplace + " as a " + job + ". ");
        }

        //birthday
        if (birthday != null) {
            builder.append("My birthday is the " + stringDate + ", which means I'm " + age + " years old. ");
        }

        //university
        if (major != null) {
            if (graduated) {
                //checks if the user already has graduated or is still studying and varies the paragraph accordingly
                builder.append("I studied " + major);
            } else {
                builder.append("I study " + major);
            }

            if (education != null) {
                builder.append(" at " + education + ".");
            } else {
                builder.append(".");
            }


        } else {
            if (education != null) {
                if (graduated) {
                    builder.append("I studied at " + education + ".");
                } else {
                    builder.append("I study at " + education + ".");
                }
            }
        }
        return builder.toString();
    }


    /**
     * This method takes the graduation date and calculates whether the person is still studying or has graduated already.
     *
     * @param date the graduation date
     * @return if the user has graduated already, returns true. If they are still studying, returns false
     */
    public Boolean getGraduation(Date date) {
        //Calendar object for the graduation date
        Calendar gradCal = Calendar.getInstance();
        gradCal.setTime(date);
        int gradYear = gradCal.get(Calendar.YEAR);
        int gradMonth = gradCal.get(Calendar.MONTH);
        int gradDay = gradCal.get(Calendar.DAY_OF_MONTH);

        //Calendar object with today's date
        Calendar today = Calendar.getInstance();
        int todayYear = today.get(Calendar.YEAR);
        int todayMonth = today.get(Calendar.MONTH);
        int todayDay = today.get(Calendar.DAY_OF_MONTH);

        //checks whether the graduation date is before or after today's date
        if (todayYear > gradYear) {
            return true;
        } else if (todayYear < gradYear) {
            return false;
        } else if (todayYear == gradYear) {
            if (todayMonth > gradMonth) {
                return true;
            } else if (todayMonth < gradMonth) {
                return false;
            } else if (todayMonth == gradMonth) {
                if (todayDay > gradDay) {
                    return true;
                } else if (todayDay < gradDay) {
                    return false;
                    //if today is the day that they have graduated, a little message is displayed to remind you of it
                } else if (todayDay == gradDay) {
                    Toast.makeText(this, R.string.graduation_time, Toast.LENGTH_SHORT).show();
                    return true;
                }
            }
        }

        return true;

    }


    /**
     * Calculates age from parameter birthday & today's date.
     *
     * @param date Friend object's birthday
     * @return a String containing the friend's age
     */
    public String getAge(Date date) {
        //get birthday info
        Calendar birthCal = Calendar.getInstance();
        birthCal.setTime(date);
        int birthYear = birthCal.get(Calendar.YEAR);
        int month = birthCal.get(Calendar.MONTH);
        int day = birthCal.get(Calendar.DAY_OF_MONTH);

        //get todays date info
        Calendar today = Calendar.getInstance();
        int todayYear = today.get(Calendar.YEAR);
        int todayMonth = today.get(Calendar.MONTH);
        int todayDay = today.get(Calendar.DAY_OF_MONTH);

        //calculate age on the day, if it's the birthday show a little reminder message
        int age = todayYear - birthYear;

        if (todayMonth < month) {
            age--;
        } else if (todayMonth == month) {
            if (todayDay < day) {
                age--;
                //if today is their birthday, displays a little message to remind you of it
            } else if (todayDay == day) {
                Toast.makeText(this, "Birthday Time!", Toast.LENGTH_SHORT).show();
            }
        }
        //ensures that no negative age can be displayed
        if (age < 0) {
            age = 0;
        }
        return String.valueOf(age);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            //camera fab clicked
            case R.id.fab_camera:
                //string containing the two options
                String[] changeProfilePictureOptions = {getString(R.string.take_a_picture), getString(R.string.from_gallery)};

                //dialog to let the user choose between new picture & picture from gallery
                new MaterialDialog.Builder(this)
                        .title(R.string.edit_picture)
                        .items(changeProfilePictureOptions)
                        .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                            @Override
                            public boolean onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
                                switch (which) {
                                    case 0: //camera

                                        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                                            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                                        }

                                        break;
                                    case 1: //gallery

                                        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                        startActivityForResult(i, PICK_IMAGE);
                                        break;
                                    default:
                                        //if something goes wrong
                                        Toast.makeText(ProfileActivity.this, R.string.error_occurred, Toast.LENGTH_SHORT).show();
                                }
                                return true;
                            }
                        })

                        .show();

                //reloads the information displayed on the page
                reload();

                break;
            //edit in about me section clicked
            case R.id.edit_about_me:
                editAboutMe();
                break;

            //edit in questions section clicked
            case R.id.edit_questions:
                editQuestions();
                break;
        }
    }


    /**
     * This method handles the return from the camera. The request code indicates whether a new photo was taken or an old one was selected from the gallery.
     *
     * @param requestCode is 1 if a new picture has been taken and 2 if an old picture has been selected from the device storage
     * @param resultCode  indicates whether something went wrong (RESULT_OK)
     * @param data        stores the new picture
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //get user from shared preferences
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        Gson gson = new Gson();
        String json = preferences.getString("LoggedInUser", "");
        LOGGEDINUSER = gson.fromJson(json, User.class);
        int position = preferences.getInt("Index", -1);

        //new picture was taken & everything went as planned
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            //get data from parameter
            Bundle extras = data.getExtras();
            //cast into a bitmap
            Bitmap bitmap = (Bitmap) extras.get("data");
            //encode to string to store
            String pic = encodeToBase64(bitmap, Bitmap.CompressFormat.JPEG, 100);

            //store in User's list
            LOGGEDINUSER.getmFriendslist().get(position).setmProfilePicture(pic);

            SharedPreferences.Editor editor = preferences.edit();
            gson = new Gson();
            json = gson.toJson(LOGGEDINUSER);
            editor.putString("LoggedInUser", json);
            editor.commit();

            //reload the page with the new information
            reload();

            //picture was picked & everything went as planned
        } else if (requestCode == PICK_IMAGE && resultCode == RESULT_OK) {
            //get image uri
            Uri pictureUri = data.getData();
            Bitmap fullSizeImage = null;
            //extract bitmap from data
            try {
                fullSizeImage = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), pictureUri);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            //encode bitmap to string to store
            String pic = encodeToBase64(fullSizeImage, Bitmap.CompressFormat.JPEG, 100);

            //store in user's list
            LOGGEDINUSER.getmFriendslist().get(position).setmProfilePicture(pic);

            SharedPreferences.Editor editor = preferences.edit();
            gson = new Gson();
            json = gson.toJson(LOGGEDINUSER);
            editor.putString("LoggedInUser", json);
            editor.commit();


            //reload the page with the new informatin
            reload();

            //if the result code was not OK, display a little message to let the user know
        } else {
            Toast.makeText(this, "Something went wrong. Please try again!", Toast.LENGTH_SHORT).show();
        }
    }




    /**
     * Encode a bitmap to base64 string so that it can be stored in the database.
     *
     * @param image          the bitmap to be converted
     * @param compressFormat the format to be compressed in
     * @param quality        quality of the resulting image
     * @return the String containing the converted bitmap
     */
    public static String encodeToBase64(Bitmap image, Bitmap.CompressFormat compressFormat, int quality) {
        ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        image.compress(compressFormat, quality, byteArrayOS);
        return Base64.encodeToString(byteArrayOS.toByteArray(), Base64.DEFAULT);
    }

    /**
     * Decode base64 encoded string into a bitmap.
     *
     * @param input the string containing the encoded bitmap
     * @return the decoded bitmap
     */
    public static Bitmap decodeBase64(String input) {
        byte[] decodedBytes = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
    }

    /**
     * This method calls a dialog to edit the additional Information. Then it takes the entered information and stores it into the user object.
     */
    private void editQuestions() {

        //get the user from the database
        final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        Gson gson = new Gson();
        String json = sharedPreferences.getString("LoggedInUser", "");
        LOGGEDINUSER = gson.fromJson(json, User.class);
        final int position = sharedPreferences.getInt("Index", -1);
        final List list = LOGGEDINUSER.getmFriendslist();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        final Friend friend = (Friend) list.get(position);

        // get the information from the database and check whether its empty or not. If it's empty, set it to "classified".
        residence = friend.getmPlaceOfResidence();
        residence = check(residence);
        phonenr = friend.getmPhonenumber();
        phonenr = check(phonenr);
        height = friend.getmHeight();
        height = check(height);
        eye = friend.getmEye();
        eye = check(eye);
        hair = friend.getmHair();
        hair = check(hair);
        film = friend.getmFilm();
        film = check(film);
        book = friend.getmBook();
        book = check(book);
        comic = friend.getmComic();
        comic = check(comic);
        game = friend.getmGame();
        game = check(game);
        pet = friend.getmPet();
        pet = check(pet);
        support = friend.getmSupporter();
        support = check(support);

        //dialog to let the user edit the info
        boolean wrapInScrollView = true;
        MaterialDialog dialog = new MaterialDialog.Builder(this)
                .title(R.string.edit_additional)
                .customView(R.layout.edit_additional, wrapInScrollView)
                .positiveText(R.string.save)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    /**
                     * The onClick is called when the "confirm" button in the dialog is pressed. It checks all fields and stores the new information to the database.
                     * @param dialog the current dialog
                     * @param which
                     */
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        //checks whether or not something has been entered
                        if (!et_residence.getText().toString().trim().isEmpty()) {
                            residence = et_residence.getText().toString().trim();
                        }

                        if (!et_phone.getText().toString().trim().isEmpty()) {
                            phonenr = et_phone.getText().toString().trim();
                        }

                        if (!et_height.getText().toString().trim().isEmpty()) {
                            height = et_height.getText().toString().trim();
                        }

                        if (!et_eye.getText().toString().trim().isEmpty()) {
                            eye = et_eye.getText().toString().trim();
                        }

                        if (!et_hair.getText().toString().trim().isEmpty()) {
                            hair = et_hair.getText().toString().trim();
                        }

                        if (!et_film.getText().toString().trim().isEmpty()) {
                            film = et_film.getText().toString().trim();
                        }

                        if (!et_book.getText().toString().trim().isEmpty()) {
                            book = et_book.getText().toString().trim();
                        }

                        if (!et_comic.getText().toString().trim().isEmpty()) {
                            comic = et_comic.getText().toString().trim();
                        }

                        if (!et_game.getText().toString().trim().isEmpty()) {
                            game = et_game.getText().toString().trim();
                        }

                        if (!et_pet.getText().toString().trim().isEmpty()) {
                            pet = et_pet.getText().toString().trim();
                        }

                        if (!et_support.getText().toString().trim().isEmpty()) {
                            support = et_support.getText().toString().trim();
                        }

                        //store information to database
                        friend.setmPlaceOfRes(residence);
                        friend.setmPhonenumber(phonenr);
                        friend.setmHeight(height);
                        friend.setmEye(eye);
                        friend.setmHair(hair);
                        friend.setmFilm(film);
                        friend.setmBook(book);
                        friend.setmComic(comic);
                        friend.setmGame(game);
                        friend.setmSupporter(support);

                        LOGGEDINUSER.getmFriendslist().set(position, friend);
                        //reload the additional-info-card

                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        Gson gson = new Gson();
                        String json = gson.toJson(LOGGEDINUSER);
                        editor.putString("LoggedInUser", json);
                        editor.commit();

                        reloadQuestions(friend);

                    }
                })
                .negativeText(R.string.cancel)
                .cancelable(true)
                .canceledOnTouchOutside(false)
                .build();

        //set up views & set the hints to the currently available information
        View v = dialog.getView();
        et_residence = (EditText) v.findViewById(R.id.et_residence);
        et_residence.setHint(residence);
        et_phone = (EditText) v.findViewById(R.id.et_phone);
        et_phone.setHint(phonenr);
        et_height = (EditText) v.findViewById(R.id.et_height);
        et_height.setHint(height);
        et_eye = (EditText) v.findViewById(R.id.et_eye_colour);
        et_eye.setHint(eye);
        et_hair = (EditText) v.findViewById(R.id.et_hair);
        et_hair.setHint(hair);
        et_film = (EditText) v.findViewById(R.id.et_film);
        et_film.setHint(film);
        et_book = (EditText) v.findViewById(R.id.et_book);
        et_book.setHint(book);
        et_comic = (EditText) v.findViewById(R.id.et_comic);
        et_comic.setHint(comic);
        et_game = (EditText) v.findViewById(R.id.et_game);
        et_game.setHint(game);
        et_pet = (EditText) v.findViewById(R.id.et_pet);
        et_pet.setHint(pet);
        et_support = (EditText) v.findViewById(R.id.et_support);
        et_support.setHint(support);

        //show the dialog
        dialog.show();

    }


    /**
     * Displays a Material Dialog with Edit Texts to edit the Basic Data. Is called if you click on the "edit" button in the "about me" section.
     * The Dialog is only cancelable by pressing "cancel"/the back button, cancel on touch outside is deactivated as to not throw away all your precious edits.
     */
    public void editAboutMe() {

//=========================get everything from the shared preferences===============================================
        final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        Gson gson = new Gson();
        String json = sharedPreferences.getString("LoggedInUser", "");
        LOGGEDINUSER = gson.fromJson(json, User.class);
        final int position = sharedPreferences.getInt("Index", -1);

        final List list = LOGGEDINUSER.getmFriendslist();
        SharedPreferences.Editor editor = sharedPreferences.edit();

        //get friend from User List
        Friend friend = (Friend) list.get(position);
        firstname = friend.getmFirstName();
        firstname = checkIfEmpty(firstname);
        lastname = friend.getmLastName();
        lastname = checkIfEmpty(lastname);
        name = friend.writeFullName();
        name = checkIfEmpty(name);
        birthday = friend.dateBirthdayGetter();
        education = friend.getmEduInstitution();
        education = checkIfEmpty(education);
        major = friend.getmEduMajor();
        major = checkIfEmpty(major);
        graduation = friend.dateEduGraduationGetter();
        workplace = friend.getmJobEmployer();
        workplace = checkIfEmpty(workplace);
        job = friend.getmJobPosition();
        job = checkIfEmpty(job);
        final Bitmap bitmap = friend.decodeBase64(friend.getmProfilePicture());
        stringDate = DateFormat.getDateInstance().format(birthday);
        stringGraduation = DateFormat.getDateInstance().format(graduation);


        boolean wrapInScrollView = true;

        //==================================MaterialDialog Edit Text===============================
        MaterialDialog dialog = new MaterialDialog.Builder(this)
                .title(R.string.edit_about_me)
                .customView(R.layout.edit_about_me, wrapInScrollView)
                .positiveText(R.string.save)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                        //=====================check for all values if anything has changed==================
                        if (!tiel_firstname.getText().toString().trim().isEmpty()) {
                            firstname = tiel_firstname.getText().toString().trim();
                        }
                        if (!tiel_lastname.getText().toString().trim().isEmpty()) {
                            lastname = tiel_lastname.getText().toString().trim();
                        }
                        if (!tiel_birthday.getText().toString().trim().isEmpty()) {
                            stringDate = tiel_birthday.getText().toString().trim();
                        }
                        if (changeDate != null) {
                            birthday = changeDate.getTime();
                        }
                        if (!tiel_education.getText().toString().isEmpty()) {
                            education = tiel_education.getText().toString();
                        }
                        if (!tiel_workplace.getText().toString().isEmpty()) {
                            workplace = tiel_workplace.getText().toString();
                        }
                        if (!tiel_jobtitle.getText().toString().isEmpty()) {
                            job = tiel_jobtitle.getText().toString();
                        }

                        if (!tiel_major.getText().toString().isEmpty()) {
                            major = tiel_major.getText().toString();
                        }

                        if (!tiel_graduation.getText().toString().trim().isEmpty()) {
                            stringGraduation = tiel_graduation.getText().toString().trim();
                        }

                        if (changeGradDate != null) {
                            graduation = changeGradDate.getTime();
                        }

                        //====================if something has changed, update the Loggedinuser====================

                        Friend friendChanged = new Friend(firstname, lastname, birthday, education, graduation, major, workplace, job, bitmap);
                        list.set(position, friendChanged);
                        LOGGEDINUSER.setmFriendslist(list);


                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        Gson gson = new Gson();
                        String json = gson.toJson(LOGGEDINUSER);
                        editor.putString("LoggedInUser", json);
                        editor.commit();


                        reload();

                    }
                })
                .negativeText(R.string.cancel)
                .cancelable(true)
                .canceledOnTouchOutside(false)
                .build();

        //set up view & datepickers
        View v = dialog.getView();
        setUpBirthdayPicker();
        setUpGraduationPicker();

        //set up the views & set the textInputEditText hints with the currently available information
        til_firstname = (TextInputLayout) v.findViewById(R.id.input_layout_first_name);
        tiel_firstname = (TextInputEditText) v.findViewById(R.id.edit_text_first_name);
        til_firstname.setHint(firstname);

        til_lastname = (TextInputLayout) v.findViewById(R.id.input_layout_last_name);
        tiel_lastname = (TextInputEditText) v.findViewById(R.id.edit_text_last_name);
        til_lastname.setHint(lastname);

        til_birthday = (TextInputLayout) v.findViewById(R.id.input_layout_birthday);
        tiel_birthday = (TextInputEditText) v.findViewById(R.id.edit_text_birthday);
        til_birthday.setHint(stringDate);
        tiel_birthday.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                setBirthdayPicker.show();
                return true;
            }
        });

        til_workplace = (TextInputLayout) v.findViewById(R.id.input_layout_workplace);
        tiel_workplace = (TextInputEditText) v.findViewById(R.id.edit_text_workplace);
        til_workplace.setHint(workplace);

        til_jobtitle = (TextInputLayout) v.findViewById(R.id.input_layout_job_title);
        tiel_jobtitle = (TextInputEditText) v.findViewById(R.id.edit_text_job_title);
        til_jobtitle.setHint(job);

        til_education = (TextInputLayout) v.findViewById(R.id.input_layout_education);
        tiel_education = (TextInputEditText) v.findViewById(R.id.edit_text_education);
        til_education.setHint(education);

        til_major = (TextInputLayout) v.findViewById(R.id.input_layout_major);
        tiel_major = (TextInputEditText) v.findViewById(R.id.edit_text_major);
        til_major.setHint(major);

        til_graduation = (TextInputLayout) v.findViewById(R.id.input_layout_graduation);
        tiel_graduation = (TextInputEditText) v.findViewById(R.id.edit_text_graduation);
        til_graduation.setHint(stringGraduation);
        tiel_graduation.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                setGraduationPicker.show();
                return true;
            }
        });

        //show the dialog
        dialog.show();
    }

    /**
     * Checks if the data from sharedPreferences has any empty strings in it. If it does, they are set to "unknown".
     *
     * @param string The string to be checked
     * @return the original String if it is not empty, "unknown" if it is
     */
    public String checkIfEmpty(String string) {
        if (string == null || string == " ") {
            string = getString(R.string.unknown);
        }
        return string;
    }

    /**
     * Sets up the date picker for the birthday.
     */
    private void setUpBirthdayPicker() {
        Calendar newCalendar = Calendar.getInstance();
        setBirthdayPicker = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                changeDate = Calendar.getInstance();
                changeDate.set(year, monthOfYear, dayOfMonth);
                tiel_birthday.setText(dateFormatter.format(changeDate.getTime()));
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
    }

    /**
     * Sets up the date picker for the graduation date.
     */
    private void setUpGraduationPicker() {
        Calendar newCalendar = Calendar.getInstance();
        setGraduationPicker = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                changeGradDate = Calendar.getInstance();
                changeGradDate.set(year, monthOfYear, dayOfMonth);
                tiel_graduation.setText(dateFormatter.format(changeGradDate.getTime()));
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
    }

    /**
     * Reloads the page with the new information
     */
    private void reload() {
        //gets information from sharedPreferences
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        Gson gson = new Gson();
        String json = sharedPreferences.getString("LoggedInUser", "");
        LOGGEDINUSER = gson.fromJson(json, User.class);
        final int position = sharedPreferences.getInt("Index", -1);
        final List list = LOGGEDINUSER.getmFriendslist();

        //get friend from User List
        Friend friend = (Friend) list.get(position);
        firstname = friend.getmFirstName();
        lastname = friend.getmLastName();
        name = friend.writeFullName();
        birthday = friend.dateBirthdayGetter();
        education = friend.getmEduInstitution();
        major = friend.getmEduMajor();
        graduation = friend.dateEduGraduationGetter();
        workplace = friend.getmJobEmployer();
        job = friend.getmJobPosition();
        stringDate = DateFormat.getDateInstance().format(birthday);
        stringGraduation = DateFormat.getDateInstance().format(graduation);
        pic = friend.getmProfilePicture();


        //reload the imageView
        reloadImage(pic);

        //reload the name shown in the collapsing toolbar
        collapsingToolbar.setTitle(name);

        //reloads the paragraph with the new information
        String paragraph = getParagraph(friend);
        tv_paragraph.setText(paragraph);

    }

    /**
     * This method reloads the imageView in the collapsing toolbar with the bitmap from the user data
     * @param pic the string containing the encoded bitmap
     */
    private void reloadImage(String pic) {
        if (pic == null) {
            //if the picture is empty, get the information from the shared preferences and set the backdrop with the picture stored there
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            Gson gson = new Gson();
            String json = sharedPreferences.getString("LoggedInUser", "");
            LOGGEDINUSER = gson.fromJson(json, User.class);
            int position = sharedPreferences.getInt("Index", -1);


            List list = LOGGEDINUSER.getmFriendslist();

            Friend friend = (Friend) list.get(position);
            final ImageView imageView = (ImageView) findViewById(R.id.backdrop);
            Glide.with(this).load(friend.getmPhotoResID()).into(imageView);
            return;
        }

        //decode the string to bitmap and set the backdrop
        Bitmap bitmap = decodeBase64(pic);
        final ImageView imageView = (ImageView) findViewById(R.id.backdrop);
        imageView.setImageDrawable(null);
        imageView.setImageBitmap(bitmap);
    }

}

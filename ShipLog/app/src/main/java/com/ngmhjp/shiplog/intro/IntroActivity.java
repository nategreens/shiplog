package com.ngmhjp.shiplog.intro;


import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;

import com.github.paolorotolo.appintro.AppIntro;
import com.google.gson.Gson;
import com.ngmhjp.shiplog.FriendsListActivity;
import com.ngmhjp.shiplog.R;
import com.ngmhjp.shiplog.model.Friend;

import java.util.Date;

/**
 * This class controls the Intro slides that get the newly registered User's basic information. It extends the AppIntro library.
 * In the OnCreate it adds the Slides and in the OnDone it calls the corresponding methods to get the information from the slides/fragments.
 */
public class IntroActivity extends AppIntro {

    //======================== SLIDES INITIALIZER ==================
    BasicFragment basicInfoSlide = new BasicFragment();
    WorkFragment workSlide = new WorkFragment();
    EduFragment eduSlide = new EduFragment();


    /**
     * The onCreate adds all the Slides and controls movement between them.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addSlide(WelcomeFragment.newInstance(R.layout.slide_welcome));
        addSlide(basicInfoSlide);
        addSlide(workSlide);
        addSlide(eduSlide);
        addSlide(DoneFragment.newInstance(R.layout.slide_done));

        showSkipButton(false);

    }


    /**
     * This method is called when the "done" button in the last fragment is pressed. It calls upon the methods of all the slides to get the information.
     * Then it writes this information into a new Friend Object and stores it in the sharedPreferences to be gotten from the FriendsListActivity. Then it starts the FriendsListActivity.
     *
     * @param currentFragment the fragment on which the done button is located
     */
    @Override
    public void onDonePressed(Fragment currentFragment) {

        //get info from slides
        String firstname = basicInfoSlide.getFirstname();
        String lastname = basicInfoSlide.getLastname();
        Date birthday = basicInfoSlide.getBirthday();
        String workplace = workSlide.getWorkplace();
        String jobtitle = workSlide.getJob();
        String university = eduSlide.getUniversity();
        String major = eduSlide.getMajor();
        Date graduation = eduSlide.getGraduation();

        //get default bitmap from resources
        Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.profile_picture_default_white);

        //make new friend with info
        Friend newFriend = new Friend(firstname, lastname, birthday, university, graduation, major, workplace, jobtitle, bm);

        //Store into sharedPreferences
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(newFriend);
        editor.putString("NewUser", json);
        editor.putBoolean("JustRegistered", true);
        editor.putBoolean("WasOffline", false);
        editor.commit();

        //start FriendsListActivity
        startActivity(new Intent(this, FriendsListActivity.class));
    }
}

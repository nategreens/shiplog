package com.ngmhjp.shiplog.intro;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.ngmhjp.shiplog.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * This fragment gets the information on the user's work career (Workplace, Job title).
 */
public class WorkFragment extends Fragment {
    //==================== Butterknife bind the views ============
    @BindView(R.id.intro_edit_text_workplace)
    TextInputEditText tiet_workplace;
    @BindView(R.id.intro_edit_text_job_title)
    TextInputEditText tiet_job_title;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    /**
     * The Views are gotten & bound
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return the view
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.slide_work, container, false);
        ButterKnife.bind(this, v);
        return v;
    }

    /**
     * This method reads the workplace from the EditText and returns it as a String.
     *
     * @return a String containing the contents of the textEdit
     */
    public String getWorkplace() {
        return tiet_workplace.getText().toString();
    }

    /**
     * This method reads the job title from the EditText and returns it as a String.
     *
     * @return a String containing the contents of the textEdit
     */
    public String getJob() {
        return tiet_job_title.getText().toString();
    }


}

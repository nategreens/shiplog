package com.ngmhjp.shiplog.intro;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;

import com.ngmhjp.shiplog.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * This fragment gets the information on the academic career of the user (university, major, graduation date)
 */
public class EduFragment extends Fragment {

    //==================== Butterknife bind the views ============
    @BindView(R.id.intro_edit_text_university)
    TextInputEditText tiet_university;
    @BindView(R.id.intro_edit_text_major)
    TextInputEditText tiet_major;
    @BindView(R.id.intro_edit_text_graduation_date)
    TextInputEditText tiet_graduation;

    //======================== SET UP FOR DATE CHOOSER ==============================
    Calendar changeDate;
    DatePickerDialog setBirthdayPicker;
    SimpleDateFormat dateFormatter = new SimpleDateFormat("dd. MMMM yyyy", Locale.US);

    /**
     * The onCreate calls setRetainInstance and the sets up the birthday picker to pick a date.
     *
     * @param savedInstanceState
     */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setUpBirthdayPicker();
    }

    /**
     * The Views are gotten & bound, setting of OnTouchListener onto the setGraduationDayPicker
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return the view
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.slide_edu, container, false);
        ButterKnife.bind(this, v);
        tiet_graduation.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                setBirthdayPicker.show();
                return false;
            }
        });
        return v;
    }

    /**
     * Sets up the date picker for the graduation date.
     */
    private void setUpBirthdayPicker() {
        //Calendar to store the date into
        Calendar newCalendar = Calendar.getInstance();
        setBirthdayPicker = new DatePickerDialog(getContext(), R.style.DialogTheme, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                changeDate = Calendar.getInstance();
                changeDate.set(year, monthOfYear, dayOfMonth);
                //set textEdit with the chosen date
                tiet_graduation.setText(dateFormatter.format(changeDate.getTime()));
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
    }


    /**
     * This method reads the university from the EditText and returns it as a String.
     *
     * @return a String containing the contents of the textEdit
     */
    public String getUniversity() {
        return tiet_university.getText().toString();
    }

    /**
     * This method reads the Field of study from the EditText and returns it as a String.
     *
     * @return a String containing the contents of the TextEdit
     */
    public String getMajor() {
        return tiet_major.getText().toString();
    }

    /**
     * This method gets the graduation date and returns it. If no graduation date has been set, set default date.
     *
     * @return the user's graduation date in a Date Object
     */
    public Date getGraduation() {
        if (changeDate == null) {
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.YEAR, 1995);
            calendar.set(Calendar.MONTH, 12);
            calendar.set(Calendar.DAY_OF_MONTH, 07);
            return calendar.getTime();
        }
        return changeDate.getTime();
    }

}

package com.ngmhjp.shiplog;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;
import com.ngmhjp.shiplog.model.Friend;
import com.ngmhjp.shiplog.model.User;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ProfileCreationActivity extends AppCompatActivity {
    private DatePickerDialog birthdayDateDialog;
    private DatePickerDialog graduationDateDialog;
    private SimpleDateFormat dateFormatter;
    private User LOGGEDINUSER;

    TextInputLayout til_firstname;
    TextInputEditText tiel_firstname;
    TextInputLayout til_lastname;
    TextInputEditText tiel_lastname;
    TextInputLayout til_birthday;
    TextInputEditText tiel_birthday;
    TextInputLayout til_workplace;
    TextInputEditText tiel_workplace;
    TextInputLayout til_jobtitle;
    TextInputEditText tiel_jobtitle;
    TextInputLayout til_education;
    TextInputEditText tiel_education;
    TextInputLayout til_educationMajor;
    TextInputEditText tiel_educationMajor;
    TextInputLayout til_educationGraduation;
    TextInputEditText tiel_educationGraduation;

    String firstname;
    String lastname;
    String birthday;
    String workplace;
    String jobtitle;
    String education;
    String educationMajor;
    String educationGraduation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_creation);

        ActionBar ab = getSupportActionBar();
        ab.setTitle("New Friend");

        // ---- Get User from previous Activity via JSON
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        Gson gson = new Gson();
        String json = sharedPreferences.getString("LoggedInUser", "");
        LOGGEDINUSER = gson.fromJson(json, User.class);

        dateFormatter = new SimpleDateFormat("dd. MMMM yyyy", Locale.US);

        til_firstname = (TextInputLayout) findViewById(R.id.newfriend_layout_first_name);
        tiel_firstname = (TextInputEditText) findViewById(R.id.newfriend_text_first_name);

        if (tiel_firstname.getText().toString().trim().isEmpty() == false) {
            firstname = tiel_firstname.getText().toString().trim();
        }

        til_firstname.setHint("First Name");

        til_lastname = (TextInputLayout) findViewById(R.id.newfriend_layout_last_name);
        tiel_lastname = (TextInputEditText) findViewById(R.id.newfriend_text_last_name);

        if (tiel_lastname.getText().toString().trim().isEmpty() == false) {
            lastname = tiel_lastname.getText().toString().trim();
        }

        til_lastname.setHint("Last Name");

        til_birthday = (TextInputLayout) findViewById(R.id.newfriend_layout_birthday);
        tiel_birthday = (TextInputEditText) findViewById(R.id.newfriend_text_birthday);

        if (tiel_birthday.getText().toString().trim().isEmpty() == false) {
            birthday = tiel_birthday.getText().toString().trim();
        }

        til_birthday.setHint("Birthday");
        tiel_birthday.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    Calendar newCalendar = Calendar.getInstance();
                    birthdayDateDialog = new DatePickerDialog(ProfileCreationActivity.this, new DatePickerDialog.OnDateSetListener() {
                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                            Calendar newDate = Calendar.getInstance();
                            newDate.set(year, monthOfYear, dayOfMonth);
                            tiel_birthday.setText(dateFormatter.format(newDate.getTime()));
                        }

                    }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

                    birthdayDateDialog.show();
                }
            }
        });

        til_workplace = (TextInputLayout) findViewById(R.id.newfriend_layout_employer);
        tiel_workplace = (TextInputEditText) findViewById(R.id.newfriend_text_employer);

        if (tiel_workplace.getText().toString().trim().isEmpty() == false) {
            workplace = tiel_workplace.getText().toString().trim();
        }

        til_workplace.setHint("Company");

        til_jobtitle = (TextInputLayout) findViewById(R.id.newfriend_layout_jobposition);
        tiel_jobtitle = (TextInputEditText) findViewById(R.id.newfriend_text_jobposition);

        if (tiel_jobtitle.getText().toString().trim().isEmpty() == false) {
            jobtitle = tiel_jobtitle.getText().toString().trim();
        }

        til_jobtitle.setHint("Job Title");

        til_education = (TextInputLayout) findViewById(R.id.newfriend_layout_education);
        tiel_education = (TextInputEditText) findViewById(R.id.newfriend_text_education);

        if (tiel_education.getText().toString().trim().isEmpty() == false) {
            education = tiel_education.getText().toString().trim();
        }

        til_education.setHint("University");

        til_educationMajor = (TextInputLayout) findViewById(R.id.newfriend_layout_educationMajor);
        tiel_educationMajor = (TextInputEditText) findViewById(R.id.newfriend_text_educationMajor);

        if (tiel_educationMajor.getText().toString().trim().isEmpty() == false) {
            educationMajor = tiel_educationMajor.getText().toString().trim();
        }

        til_educationMajor.setHint("Major");
        til_educationGraduation = (TextInputLayout) findViewById(R.id.newfriend_layout_educationGraduation);
        tiel_educationGraduation = (TextInputEditText) findViewById(R.id.newfriend_text_educationGraduation);

        if (tiel_educationGraduation.getText().toString().trim().isEmpty() == false) {
            educationGraduation = tiel_educationGraduation.getText().toString().trim();
        }

        til_educationGraduation.setHint("Graduation Date");
        tiel_educationGraduation.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    Calendar newCalendar = Calendar.getInstance();
                    graduationDateDialog = new DatePickerDialog(ProfileCreationActivity.this, new DatePickerDialog.OnDateSetListener() {
                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                            Calendar newDate = Calendar.getInstance();
                            newDate.set(year, monthOfYear, dayOfMonth);
                            tiel_educationGraduation.setText(dateFormatter.format(newDate.getTime()));
                        }

                    }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
                    graduationDateDialog.show();
                }
            }
        });


        Button save = (Button) findViewById(R.id.button_saveProfile);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveProfile();
            }
        });
    }

    public String checkIfEmpty(String string) {
        if (string == null || string == "") {
            string = getString(R.string.unknown);
        }
        return string;
    }

    private void saveProfile() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Creating new Friend");
        progressDialog.show();
        try {
            //get Database reference
            DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();

            //get Data from input fields
            String firstName = tiel_firstname.getText().toString().trim();
            String lastName = tiel_lastname.getText().toString().trim();
            Date birthday = dateFormatter.parse(tiel_birthday.getText().toString());
            String employer = tiel_workplace.getText().toString();
            String jobtitle = tiel_jobtitle.getText().toString();
            String education = tiel_education.getText().toString();
            String educationMajor = tiel_educationMajor.getText().toString();
            Date graduation = dateFormatter.parse(tiel_educationGraduation.getText().toString());

            //Create new Friend Object, append to List, push List to Firebase
            Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.profile_picture_default_white);

            Friend newFriend = new Friend(firstName, lastName, birthday, education, graduation, educationMajor, employer, jobtitle, icon);

            saveFriendToSharedPrefs(newFriend);
            List<Friend> updatedFriendslist = LOGGEDINUSER.getmFriendslist();
            updatedFriendslist.add(newFriend);

            mDatabase.child("users").child(LOGGEDINUSER.getmUID()).child("mFriendslist").setValue(updatedFriendslist).addOnCompleteListener(this, new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) {
                        Log.i("Profile Creation", "Friend created with Basic Data and pushed to Firebase!");
                    } else {
                        Log.e("Profile Creation", "Friend creation FAILED!");

                    }
                }
            });
            progressDialog.dismiss();

            // --- Create Intent to return to FriendsListActivity and replace it's LOGGEDINUSER Object

            Intent returnIntent = new Intent();
            Gson gson = new Gson();
            String json = gson.toJson(LOGGEDINUSER);
            returnIntent.putExtra("LoggedInUserJSON", json);
            setResult(Activity.RESULT_OK, returnIntent);
            finish();

        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    private void saveFriendToSharedPrefs(Friend _friend) {
        SharedPreferences mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(_friend);
        editor.putString("NewFriend", json);
        editor.commit();
    }
}

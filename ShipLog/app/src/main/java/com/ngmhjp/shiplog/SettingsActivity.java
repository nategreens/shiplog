package com.ngmhjp.shiplog;

import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class SettingsActivity extends AppCompatActivity {
    FirebaseUser mUser = FirebaseAuth.getInstance().getCurrentUser();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        // Get a support ActionBar and enable Up/Back Button
        ActionBar ab = getSupportActionBar();
        ab.setTitle("Settings");
        ab.setDisplayHomeAsUpEnabled(true);

        //TODO: use Strings XML and update switch case below (indices perhaps?)
        String[] settings = new String[]{"Change E-Mail",
                "Change Password",
                "Help/FAQ"
        };

        final ListView listView = (ListView) findViewById(R.id.settings_list);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, settings);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                switch ((String) listView.getItemAtPosition(position)) {
                    case "Change E-Mail": {
                        new MaterialDialog.Builder(SettingsActivity.this)
                                .title("Enter Password")
                                .content("Before changing your E-Mail, you must enter your Password")
                                .inputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD)
                                .input("Current Password", "", new MaterialDialog.InputCallback() {
                                    @Override
                                    public void onInput(MaterialDialog dialog, CharSequence input) {
                                        AuthCredential credential = EmailAuthProvider.getCredential(mUser.getEmail(), input.toString());
                                        mUser.reauthenticate(credential)
                                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if (task.isSuccessful()) {
                                                            Log.i("Settings:", "User re-authenticated.");
                                                            initFirebaseDataChange(false);
                                                        } else {
                                                            Log.e("Settings:", "User re-authentification FAILED.");
                                                            Toast.makeText(SettingsActivity.this, "Wrong Password/No Internet Connectivity. Please try again.", Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                });
                                    }
                                })
                                .positiveText("Continue")
                                .negativeText("Cancel")
                                .show();
                        break;
                    }
                    case "Change Password": {
                        new MaterialDialog.Builder(SettingsActivity.this)
                                .title("Enter Password")
                                .content("Enter your old password in order to continue")
                                .inputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD)
                                .input("Old Password", "", new MaterialDialog.InputCallback() {
                                    @Override
                                    public void onInput(MaterialDialog dialog, CharSequence input) {
                                        AuthCredential credential = EmailAuthProvider.getCredential(mUser.getEmail(), input.toString());
                                        mUser.reauthenticate(credential)
                                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if (task.isSuccessful()) {
                                                            Log.i("Settings:", "User re-authenticated.");
                                                            initFirebaseDataChange(true);
                                                        } else {
                                                            Log.e("Settings:", "User re-authentification FAILED.");
                                                            Toast.makeText(SettingsActivity.this, "Wrong Password/No Internet Connectivity. Please try again.", Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                });
                                    }
                                })
                                .positiveText("Continue")
                                .negativeText("Cancel")
                                .show();
                        break;
                    }
                    case "Help/FAQ": {
                        Toast.makeText(SettingsActivity.this, "You're fucked!", Toast.LENGTH_SHORT).show();
                        break;
                    }
                }

            }
        });
    }

    /*
        Re-authenticate the user prior to calling this method!
        Changes Firebase User's E-Mail or Password (define in parameter)
        @param changePassword if true, dialog for changing the user's password will be displayed, if false, change user's E-Mail
     */
    private void initFirebaseDataChange(boolean changePassword) {
        if (changePassword) {
            new MaterialDialog.Builder(SettingsActivity.this)
                    .title("Change Password")
                    .content("Enter your new Password")
                    .inputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD)
                    .input("New Password", "", new MaterialDialog.InputCallback() {
                        @Override
                        public void onInput(MaterialDialog dialog, CharSequence input) {
                            String newPassword = input.toString();
                            Log.i("Settings:", "Password entered '" + newPassword + "'" );
                            mUser.updatePassword(newPassword).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        Log.i("Settings:", "User password updated.");
                                        Toast.makeText(SettingsActivity.this, "Password updated successfully! :)", Toast.LENGTH_SHORT).show();
                                    } else {
                                        Log.e("Settings:", "User password update FAILED.");
                                        Toast.makeText(SettingsActivity.this, "Something went wrong :(", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });;
                        }
                    })
                    .positiveText("Change")
                    .negativeText("Cancel")
                    .show();        } else {
            new MaterialDialog.Builder(SettingsActivity.this)
                    .title("Change E-Mail")
                    .content("Enter your new E-Mail address")
                    .inputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS)
                    .input("E-Mail address", "", new MaterialDialog.InputCallback() {
                        @Override
                        public void onInput(MaterialDialog dialog, CharSequence input) {
                            String email = input.toString();
                            Log.i("Settings:", "E-Mail entered '" + email + "'" );
                            mUser.updateEmail(email).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        Log.d("Settings:", "User E-Mail address updated.");
                                        Toast.makeText(SettingsActivity.this, "E-Mail updated successfully! :)", Toast.LENGTH_SHORT).show();
                                    } else {
                                        Log.e("Settings:", "User E-Mail update FAILED.");
                                        Toast.makeText(SettingsActivity.this, "Something went wrong :(", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });;
                        }
                    })
                    .positiveText("Change")
                    .negativeText("Cancel")
                    .show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);


    }

}

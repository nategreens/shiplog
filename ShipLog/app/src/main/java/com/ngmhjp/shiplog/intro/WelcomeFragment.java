package com.ngmhjp.shiplog.intro;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * This is the first fragment.
 */
public class WelcomeFragment extends Fragment {

    //===================== LAYOUT FILE ID MEMBER =====================
    private static final String ARG_LAYOUT_RES_ID = "layoutResId";
    private int layoutResId;

    /**
     * Initializes a new AppIntro fragment from the layout resource and returns it
     * @param layoutResId id of the layout resource file for the fragment
     * @return the newly initialized fragment
     */
    public static WelcomeFragment newInstance(int layoutResId) {
        WelcomeFragment fragment = new WelcomeFragment();

        Bundle args = new Bundle();
        args.putInt(ARG_LAYOUT_RES_ID, layoutResId);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null && getArguments().containsKey(ARG_LAYOUT_RES_ID)) {
            layoutResId = getArguments().getInt(ARG_LAYOUT_RES_ID);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(layoutResId, container, false);
    }


}

package com.ngmhjp.shiplog.intro;

import android.app.DatePickerDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;

import com.github.paolorotolo.appintro.ISlideBackgroundColorHolder;
import com.ngmhjp.shiplog.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * This fragment gets the basic information from the user (name & birthday) and returns it.
 */
public class BasicFragment extends Fragment {

//==================== Butterknife bind the views ============
    @BindView(R.id.intro_edit_text_first_name)
    TextInputEditText tiet_firstname;
    @BindView(R.id.intro_edit_text_last_name)
    TextInputEditText tiet_lastname;
    @BindView(R.id.intro_edit_text_birthday)
    TextInputEditText tiet_birthday;

//======================== SET UP FOR DATE CHOOSER ==============================
    DatePickerDialog setBirthdayPicker;
    Calendar changeDate;
    SimpleDateFormat dateFormatter = new SimpleDateFormat("dd. MMMM yyyy", Locale.US);


    /**
     * The onCreate calls setRetainInstance and the sets up the birthday picker to pick a date.
     * @param savedInstanceState
     */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setUpBirthdayPicker();
    }

    /**
     * The Views are gotten & bound, setting of OnTouchListener onto the setBirthdayPicker
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return the view
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.slide_name, container, false);
        ButterKnife.bind(this, v);
        tiet_birthday.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                setBirthdayPicker.show();
                return false;
            }
        });

        return v;

    }

    /**
     * Sets up the date picker for the birthday.
     */
    private void setUpBirthdayPicker() {
        //Calendar to store the date into
        Calendar newCalendar = Calendar.getInstance();
        setBirthdayPicker = new DatePickerDialog(getContext(), R.style.DialogTheme, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                changeDate = Calendar.getInstance();
                changeDate.set(year, monthOfYear, dayOfMonth);
                if (changeDate == null) {
                    changeDate.set(1995, 12, 07);
                }
                //set textEdit with the chosen date
                tiet_birthday.setText(dateFormatter.format(changeDate.getTime()));
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
    }


    /**
     * This method reads the firstname from the editText and returns it as a string.
     * @return String containing contents of textEdit
     */
    public String getFirstname() {
        return tiet_firstname.getText().toString().trim();
    }

    /**
     * This method gets the birthday and returns it. If no birthday has been set, set default date.
     * @return the user's birthday in a Date object
     */
    public Date getBirthday() {
        if (changeDate == null) {
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.YEAR, 1995);
            calendar.set(Calendar.MONTH, 12);
            calendar.set(Calendar.DAY_OF_MONTH, 07);
            return calendar.getTime();
        }
        return changeDate.getTime();
    }

    /**
     * This method reads the lastname from the textEdit and returns it as a String
     * @return a String containing contents od lastname textEdit
     */
    public String getLastname() {
        return tiet_lastname.getText().toString().trim();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}





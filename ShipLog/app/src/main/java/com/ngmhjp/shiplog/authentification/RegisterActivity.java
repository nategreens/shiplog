package com.ngmhjp.shiplog.authentification;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.ngmhjp.shiplog.FriendsListActivity;
import com.ngmhjp.shiplog.R;
import com.ngmhjp.shiplog.intro.IntroActivity;
import com.ngmhjp.shiplog.model.User;


/**
 * This class coordinates the registration of a new user to the firebase. It gets all the information from the
 * userInput and registers the user via firebaseAuth. It also checks whether the inputFields are empty.
 */
public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    //=============================== MEMBER =================================
    private FirebaseAuth firebaseAuth;
    private Button buttonRegister;
    private EditText editTextEmail;
    private EditText getEditTextPassword;
    private TextView textViewLogin;
    private ProgressDialog progressDialog;
    private DatabaseReference mDatabase;


    /**
     * The OnCreate creates a new firebaseAuth, sets the background photo of the activity and handles the views.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        //FirebaseAuth = firebaselogin. Checks whether you're logged in already.
        firebaseAuth = FirebaseAuth.getInstance();
        if (firebaseAuth.getCurrentUser() != null) {
            finish();
            startActivity(new Intent(getApplicationContext(), FriendsListActivity.class));
        }

        //set background photo for acitivity
        ImageView mBackgroundPhoto = (ImageView) findViewById(R.id.registerImageBackground);
        Glide.with(this).load(R.drawable.login_photo2).into(mBackgroundPhoto);

        //View Handling
        buttonRegister = (Button) findViewById(R.id.button_register);
        editTextEmail = (EditText) findViewById(R.id.edit_text_register_email);
        getEditTextPassword = (EditText) findViewById(R.id.edit_text_register_password);
        textViewLogin = (TextView) findViewById(R.id.textView_register_sign_in);
        progressDialog = new ProgressDialog(this);
        buttonRegister.setOnClickListener(this);
        textViewLogin.setOnClickListener(this);

        //if you come from SignInActivity and you've entered an email already, this gets the email and puts it into the text field so you don't have to write it a second time
        String emailExtra = getIntent().getStringExtra("Email");
        editTextEmail.setText(emailExtra);
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.button_register:
                registerUser();
                break;
            case R.id.textView_register_sign_in:
                String email = editTextEmail.getText().toString().trim();
                //no finish, because history set to false
                startActivity(new Intent(getApplicationContext(), SignInActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION).putExtra("Email", email));
                break;
        }
    }

    /**
     * This method serves to register the user to the firebase and call the FriendListActivity.
     */
    private void registerUser() {
        //reads user input
        String email = editTextEmail.getText().toString().trim();
        String password = getEditTextPassword.getText().toString().trim();

        //creates new User Object (only ONE for the whole application)
        final User user = new User(this, email);

        //if no email has been entered
        if (TextUtils.isEmpty(email)) {
            Toast.makeText(this, R.string.email_empty, Toast.LENGTH_SHORT).show();
            return;
        }

        //if no password has been entered
        if (TextUtils.isEmpty(password)) {
            Toast.makeText(this, R.string.password_empty, Toast.LENGTH_SHORT).show();
            return;
        }

        //progress dialog to prevent the user from having to look at the activity while it's registering
        progressDialog.setMessage(this.getResources().getString(R.string.registering_user));
        progressDialog.show();

        //create new User
        firebaseAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                //if everything works, finish this activity & start IntroActivity to set up the basic info
                if (task.isSuccessful()) {
                    user.setmUID(FirebaseAuth.getInstance().getCurrentUser().getUid());
                    mDatabase = FirebaseDatabase.getInstance().getReference();
                    mDatabase.child("users").child(user.getmUID()).setValue(user);

                    finish();
                    startActivity(new Intent(getApplicationContext(), IntroActivity.class));

                } else {
                    //if it doesn't work, dismiss the progressDialog and let the user know that it didn't work
                    progressDialog.dismiss();
                    Toast.makeText(RegisterActivity.this, R.string.registration_failed, Toast.LENGTH_SHORT).show();

                }

            }
        });
    }
}

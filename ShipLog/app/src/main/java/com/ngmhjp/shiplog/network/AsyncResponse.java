package com.ngmhjp.shiplog.network;

import com.ngmhjp.shiplog.model.Friend;
import com.ngmhjp.shiplog.model.User;

import java.util.List;

/**
 * This interface is the connection between the async download task and the main UI thread in FriendsListActivity
 * Created by Jordan on 08/02/2017.
 */

public interface AsyncResponse {
    void processFinish(User output);
}

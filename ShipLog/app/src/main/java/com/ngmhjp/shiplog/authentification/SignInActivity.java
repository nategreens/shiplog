package com.ngmhjp.shiplog.authentification;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.ngmhjp.shiplog.FriendsListActivity;
import com.ngmhjp.shiplog.R;

/**
 * This class coordinated the userLogin to the firebase. It gets all the information from the userInput and checks if the user exists and then logs in.
 */
public class SignInActivity extends AppCompatActivity implements View.OnClickListener {

    //=========================== MEMBER ==============================
    private FirebaseAuth firebaseAuth;
    private Button buttonSignIn;
    private EditText editTextEmail;
    private EditText editTextPassword;
    private TextView textViewSignIn;
    private ProgressDialog progressDialog;


    /**
     * The OnCreate creates a new firebaseAuth, sets the background photo of the activity and handles the views.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        //set activity background picture
        ImageView mBackgroundPhoto = (ImageView) findViewById(R.id.loginImageBackground);
        Glide.with(this).load(R.drawable.login_photo2).into(mBackgroundPhoto);

        //checks if you're logged in already
        firebaseAuth = FirebaseAuth.getInstance();
        if (firebaseAuth.getCurrentUser() != null) {
            finish();
            startActivity(new Intent(getApplicationContext(), FriendsListActivity.class));
        }

        //View Handling
        buttonSignIn = (Button) findViewById(R.id.button_sign_in);
        editTextEmail = (EditText) findViewById(R.id.edit_text_sign_in_email);
        editTextPassword = (EditText) findViewById(R.id.edit_text_sign_in_password);
        textViewSignIn = (TextView) findViewById(R.id.textView_sign_in_register);
        progressDialog = new ProgressDialog(this);
        buttonSignIn.setOnClickListener(this);
        textViewSignIn.setOnClickListener(this);

        //if you come from RegisterActivity and you've entered an email already, this gets the email and puts it into the text field so you don't have to write it a second time
        String emailExtra = getIntent().getStringExtra("Email");
        editTextEmail.setText(emailExtra);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_sign_in:
                userLogin();

                break;
            case R.id.textView_sign_in_register:
                String email = editTextEmail.getText().toString().trim();
                //no finish, because history set to false
                startActivity(new Intent(getApplicationContext(), RegisterActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION).putExtra("Email", email));
                break;
        }
    }

    /**
     * This method reads the user input and tries the login. If it doesn't work it lets the user know. If everything works it calls the LogInActivity
     */
    private void userLogin() {
        //reads user input
        String email = editTextEmail.getText().toString().trim();
        String password = editTextPassword.getText().toString().trim();

        //if no email has been entered
        if (TextUtils.isEmpty(email)) {
            //TODO: Snackbar email empty
            Toast.makeText(this, R.string.email_empty, Toast.LENGTH_SHORT).show();
            return;
        }

        //if no password has been entered
        if (TextUtils.isEmpty(password)) {
            //TODO: Snackbar password empty
            Toast.makeText(this, R.string.password_empty, Toast.LENGTH_SHORT).show();
            return;
        }

        //progress dialog to prevent the user from having to look at the activity while it's registering
        progressDialog.setMessage(this.getResources().getString(R.string.signing_in));
        progressDialog.show();

        //logs in the user to the firebase
        firebaseAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                //gets rid of progress dialog
                progressDialog.dismiss();
                if (task.isSuccessful()) {
                    //if everything works, activity finishes & FriendsListActivity starts
                    finish();
                    startActivity(new Intent(getApplicationContext(), FriendsListActivity.class));
                } else {
                    //if it doesn't work, dismiss progressDialog and let the user know that it didn't work
                    progressDialog.dismiss();
                    Toast.makeText(SignInActivity.this, R.string.sign_in_failed, Toast.LENGTH_SHORT).show();
                }

            }
        });

    }
}

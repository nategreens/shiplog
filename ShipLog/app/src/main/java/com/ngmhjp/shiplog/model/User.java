package com.ngmhjp.shiplog.model;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import com.ngmhjp.shiplog.FriendsListActivity;
import com.ngmhjp.shiplog.R;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Jordan on 31.01.17.´
 */

public class User {
    public List<Friend> mFriendslist = new ArrayList<Friend>();
    private String mEmail = "";
    private String mUID = "";



    public List<Friend> getmFriendslist(){
        return mFriendslist;
    }

    public User() {}

    public User(Context context, String _email, String _uid) {
        this.mEmail = _email;
        this.mUID = _uid;
        mFriendslist = new ArrayList<Friend>();
    }


        public User(Context context, String _email) {
        this.mEmail = _email;
        mFriendslist = new ArrayList<Friend>();

        //--- Dummy Data for testing
        Date dummyDate = new Date();

        //exact Date for more testing
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 1997);
        calendar.set(Calendar.MONTH, 10);
        calendar.set(Calendar.DAY_OF_MONTH, 21);
        Date date = calendar.getTime();
        Bitmap bm_default = BitmapFactory.decodeResource(context.getResources(), R.drawable.profile_picture_default);


        //Friend dummyFriend1 = new Friend("Jordan", "Pichler", dummyDate, "FH OÖ", dummyDate, "Mobile Computing", "Massimo Dutti", "Fashion Advisor", R.drawable.photo_jordan);
       // Friend dummyFriend2 = new Friend("Magdalena", "Hametner", dummyDate, "FH OÖ", dummyDate, "Mobile Computing", "Zara", "Visual Merchandiser", "Lindenstraße 19", "54509", "167", "blue", "fair", "Harry Potter", "Harry Potter", "Detectiv Conan", "Sims3", "Cat Moritz", "Prinzen", bm_default);
        //Friend dummyFriend3 = new Friend("Nathan", "Grinzinger", date, "FH OÖ", dummyDate, "Mobile Computing", "Lifepole Inc.", "Developer", R.drawable.photo_nathan);
        //Friend dummyFriend4 = new Friend("John", "Appleseed", dummyDate, "FH OÖ", dummyDate, "Mobile Computing", "Stradivarius", "Floor Assistant", R.drawable.photo_appleseed);
        //Friend dummyFriend5 = new Friend("Tom", "Daley", dummyDate, "FH OÖ", dummyDate, "Mobile Computing", "Pull & Bear", "Floor Assistant", R.drawable.photo_daley);
        //Friend dummyFriend6 = new Friend("Beyoncé", "Knowles", dummyDate, "FH OÖ", dummyDate, "Mobile Computing", "Massimo Dutti", "Floor Assistant", R.drawable.photo_beyonce);
        //Friend dummyFriend7 = new Friend("Taylor", "Swift", dummyDate, "FH OÖ", dummyDate, "Mobile Computing", "Zara", "Floor Assistant", R.drawable.photo_swift);

        //mFriendslist.add(dummyFriend1);
       // mFriendslist.add(dummyFriend2);
        /*mFriendslist.add(dummyFriend3);
        mFriendslist.add(dummyFriend4);
        mFriendslist.add(dummyFriend5);
        mFriendslist.add(dummyFriend6);
        mFriendslist.add(dummyFriend7);*/

        //--- ----


    }

    public String getmEmail() {
        return mEmail;
    }

    public void setmEmail(String mEmail) {
        this.mEmail = mEmail;
    }

    public void setmFriendslist(List<Friend> list){
        mFriendslist = (ArrayList<Friend>) list;

    }

    public String getmUID() {
        return mUID;
    }

    public void setmUID(String mUID) {
        this.mUID = mUID;
    }

    public static String encodeToBase64(Bitmap image, Bitmap.CompressFormat compressFormat, int quality) {
        ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        image.compress(compressFormat, quality, byteArrayOS);
        return Base64.encodeToString(byteArrayOS.toByteArray(), Base64.DEFAULT);
    }
}

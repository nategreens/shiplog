package com.ngmhjp.shiplog.network;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ngmhjp.shiplog.model.Friend;
import com.ngmhjp.shiplog.model.User;

import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jordan on 08/02/2017.
 */

public class FirebaseAsyncTask extends AsyncTask<String, Void, User> implements AsyncResponse {
    public AsyncResponse delegate = null;


    @Override
    protected User doInBackground(String... params) {
        User downloadedUser = new User();
        if (params.length == 2) {
            Log.i("FirebaseAsyncTask", "Params correct, creating URL");
            HttpRequest request = HttpRequest.get("https://friendsbook-5c500.firebaseio.com/users/" + params[0] + "/mFriendslist.json");
            String response = request.body();
            int code = request.code();
            Log.i("FirebaseAsyncTask", "Return Code: " + code);
            Gson gson = new Gson();

            //Parse JSON Response
            Type listType = new TypeToken<ArrayList<Friend>>(){}.getType();
            List<Friend> downloadedFriendslist = gson.fromJson(response, listType);

            downloadedUser.setmUID(params[0]);
            downloadedUser.setmEmail(params[1]);
            downloadedUser.setmFriendslist(downloadedFriendslist);
        }

        return downloadedUser;
    }

    @Override
    protected void onPostExecute(User user) {
        delegate.processFinish(user);
    }

    @Override
    public void processFinish(User output) {
        //Implemented in FriendsListActivity.class
    }
}

package com.ngmhjp.shiplog.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ngmhjp.shiplog.R;
import com.ngmhjp.shiplog.model.Friend;

import java.util.List;


/**
 * Manages the RecyclerView List on FriendsListActivity
 * Get's Data from LOGGEDINUSER
 * Created by Jordan on 06.01.2017
 */
public class
RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.FriendViewHolder> {
    private Context mContext;
    private List<Friend> mListdata;
    private LayoutInflater mInflater;

    public RecyclerAdapter(List<Friend> listdata, Context c) {
        mContext = c;
        mInflater = LayoutInflater.from(mContext);
        mListdata = listdata;
    }

    @Override
    public RecyclerAdapter.FriendViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.friendslist_item, parent, false);
        return new FriendViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FriendViewHolder holder, int position) {
        Friend item = mListdata.get(position);
        holder.mNameLabel.setText(item.writeFullName());
        //Glide.with(mContext).load(item.getmPhotoResID()).into(holder.mPhoto);
        holder.mPhoto.setImageDrawable(null);
        holder.mPhoto.setImageBitmap(item.decodeBase64(item.getmProfilePicture()));
    }

    @Override
    public int getItemCount() {
        return mListdata.size();
    }


    // ---- ViewHolder for RecyclerView (Representation of list entries)

    public static class FriendViewHolder extends RecyclerView.ViewHolder {
        private TextView mNameLabel;
        private ImageView mPhoto;
        private View mContainer;

        public FriendViewHolder(View v) {
            super(v);
            mNameLabel = (TextView) v.findViewById(R.id.listitem_name);
            mPhoto = (de.hdodenhof.circleimageview.CircleImageView) v.findViewById(R.id.listitem_photo);
            mContainer = v.findViewById(R.id.listitem_root);
        }

    }

}
